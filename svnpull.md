# snvpull.py

The `snvpull.py` script is maintained on the `svnpull` branch. It
has been removed from the `master` branch on purpose.

Graeme, 2018-08-23
